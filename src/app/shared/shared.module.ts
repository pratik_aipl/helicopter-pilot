import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthGuard } from './authguard.service';
import { DatePipe } from '@angular/common';



@NgModule({
  // declarations: [TranslateModule],
  imports: [
    CommonModule,
    IonicModule,
    HttpClientModule,
    ReactiveFormsModule
    ],
  exports: [CommonModule, IonicModule,  ReactiveFormsModule],
  providers: [AuthGuard,  DatePipe]
})
export class SharedModule { }
