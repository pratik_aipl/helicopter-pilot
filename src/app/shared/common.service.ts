import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';
import { Tools } from '../tools';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  deviceInfo;
  bacisAuth;
  httpOptions: any;
  
  constructor(public auth: AuthGuard, public http: HttpClient, public tool: Tools) {
    this.deviceInfo = this.getDeviceInfo();
    this.bacisAuth = 'Basic ' + btoa(environment.username + ":" + environment.password);
  }
  login(email, password): any {
    let dID;
    if (localStorage.getItem('PlayerID') != undefined) {
      dID = localStorage.getItem('PlayerID') == '' ? "111111" : localStorage.getItem('PlayerID');
    } else {
      dID = '111111'
    }

   var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
      })
    }

    let postData = new FormData();
    // postData.append('file', imageFile);
    postData.append("EmailID", email);
    postData.append("Password", password);
    postData.append("DeviceID", dID);

    return this.http.post(environment.BaseUrl + 'login', postData,httpOptions);
  }
  getRideList(): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),

      })
    }
    return this.http.get(environment.BaseUrl + 'RideList',  httpOptions);
  }
  userinfo(): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),

      })
    }
    return this.http.get(environment.BaseUrl + 'userinfo', httpOptions);
  }

   getRideHistory(): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),

      })
    }
      return this.http.get(environment.BaseUrl + 'MyRideList',  httpOptions);
    }
   changeStatus(ride_id): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
        'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
        'User-Id': this.getUserId(),

      })
    }
      return this.http.get(environment.BaseUrl + 'changed_status/'+ride_id,httpOptions);
    }
  getCurrentLatLng() {
    navigator.geolocation.getCurrentPosition((res) => {
      let currentLatLng: any = {};
      currentLatLng.latitude = res.coords.latitude;
      currentLatLng.longitude = res.coords.longitude;
      return currentLatLng;
    });
  }

  // GET & SET USER DATA
  setUserData(userData, login_token) {
    console.log(userData)
    window.localStorage.setItem('pil_data', JSON.stringify(userData));
    if(login_token !='')
    window.localStorage.setItem('pil_token', login_token);
    window.localStorage.setItem('pil_id', userData.user.id);
  }

  setSelectedRoute(item) {
    window.localStorage.setItem('sel_route', JSON.stringify(item));
  }
  getSelectedRoute() {
    if (window.localStorage['sel_route']) {
      return JSON.parse(window.localStorage['sel_route']);
    }
    return;
  }
  setSelectedPass(item) {
    if (item != null)
      window.localStorage.setItem('selPass', JSON.stringify(item));
    else
      window.localStorage.setItem('selPass', '');
  }
  getSelectedPass() {
    if (window.localStorage['selPass']) {
      return JSON.parse(window.localStorage['selPass']);
    }
    return;
  }
  getUserData() {
    if (window.localStorage['pil_data']) {
      return JSON.parse(window.localStorage['pil_data']);
    }
    return;
  }
  getUserId() {
    if (window.localStorage['pil_id']) {
      return window.localStorage['pil_id'];
    }
    return;
  }

  getLoginToken() {
    if (window.localStorage['pil_token']) {
      return window.localStorage['pil_token'];
    }
    return;
  }


  // GET & SET DEVICE INFO
  setDeviceInfo(deviceInfo) {
    window.localStorage.setItem('deviceInfo', JSON.stringify(deviceInfo));
  }

  getDeviceInfo() {
    if (window.localStorage['deviceInfo']) {
      return JSON.parse(window.localStorage['deviceInfo']);
    }
    return;
  }
}
