import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate() {
    if (this.loggedIn()) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }

  loggedIn() {
    if (localStorage.getItem('pil_data') === null) {
      return false;
    }
    return true;
  }
}
