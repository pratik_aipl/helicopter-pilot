import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/authguard.service';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'dashboard', canActivate: [AuthGuard],loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule' },
  { path: 'route-details',canActivate: [AuthGuard], loadChildren: './pages/route-details/route-details.module#RouteDetailsPageModule' },
  { path: 'ride-history', canActivate: [AuthGuard],loadChildren: './pages/ride-history/ride-history.module#RideHistoryPageModule' },
  { path: 'profile', canActivate: [AuthGuard],loadChildren: './pages/profile/profile.module#ProfilePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
