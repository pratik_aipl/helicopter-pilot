import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) { 
      this.tools.callOneSignal();
      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],//pilot@gmail.com
        password: ['', Validators.required],//123456
      });
    }

  ngOnInit() {
  }

  onSubmit() {
    this.tools.openLoader();
    let email = this.loginForm.get('email').value;
    let password = this.loginForm.get('password').value;
    this.commonService.login(email, password).subscribe(response => {
      this.tools.closeLoader();     
      let res: any = response;
      console.log('Login Details --> ',res);
      this.loginForm.reset();
      this.commonService.setUserData(res.data,res.login_token);
      this.router.navigateByUrl('/dashboard');
    }, (error: Response) => {
      this.tools.closeLoader();
      console.log(error);
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }
  // goToHome() {
  //   this.router.navigateByUrl('/dashboard');
  // }
}
