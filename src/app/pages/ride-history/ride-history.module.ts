import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RideHistoryPage } from './ride-history.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: RideHistoryPage
  }
];

@NgModule({
  imports: [
   SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RideHistoryPage]
})
export class RideHistoryPageModule {}
