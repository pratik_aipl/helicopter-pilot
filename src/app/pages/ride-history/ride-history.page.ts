import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tools } from 'src/app/tools';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-ride-history',
  templateUrl: './ride-history.page.html',
  styleUrls: ['./ride-history.page.scss'],
})
export class RideHistoryPage implements OnInit {

  rideList = [];
  today_ride_list = [];
  constructor(public router: Router,public tools:Tools,public commonService:CommonService) { 

  }
  ngOnInit() {
  
  }
  ionViewWillEnter(){
    console.log('ionViewWillEnter --1 ');
  }
  ionViewDidEnter(){
    console.log('ionViewDidEnter --2 ');
    this.getRideHistorylist();
  }
  getRideHistorylist() {
    this.tools.openLoader();
    this.commonService.getRideHistory().subscribe(response => {
      this.tools.closeLoader();
      let res: any = response;
      console.log('Ride History --> ',res);
      
      this.rideList=(res.ride_list);
    }, (error: Response) => {
      this.tools.closeLoader();
      let err:any = error;
      console.log(err);
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }
  rideClick(item) {
    this.commonService.setSelectedRoute(item)
    this.router.navigateByUrl('/route-details');
  }
  
 


}
