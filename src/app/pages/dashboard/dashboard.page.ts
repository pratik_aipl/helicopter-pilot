import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { Tools } from 'src/app/tools';
import { CommonService } from 'src/app/shared/common.service';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  scannedResult: any;

  readyToScan: boolean = false;
  scanning: boolean = false;
  isFirst = false;
  user: any = {};
  routeList: any = [];
  today_ride_list: any = [];
  intervalHandle: any = null;


  constructor(public router: Router, public tools: Tools, private menu: MenuController, public commonService: CommonService, private qrScanner: QRScanner) {
    this.user = this.commonService.getUserData();

  }

  ngOnDestroy(): void {
    if (this.intervalHandle != null)
      clearInterval(this.intervalHandle);
    this.intervalHandle = null;
  }
  ionViewWillEnter() {
    this.isFirst = false;
    this.getRouteList();
  }
  ionViewDidEnter() {
    this.isFirst = false;
    console.log('ionViewDidEnter');
    if (this.intervalHandle === null) {
      this.intervalHandle = setInterval(() => {
        this.getRouteList();
      }, 10000);
    } else {
      clearInterval(this.intervalHandle);
      this.intervalHandle = null;
    }
  }
  ionViewDidLeave() {
    console.log('ionViewDidLeave');
    if (this.intervalHandle != null)
      clearInterval(this.intervalHandle);
    this.intervalHandle = null;
  }

  ngOnInit() {

  }

  getRouteList() {
    this.routeList = [];
    if (!this.isFirst) {
      this.isFirst = true
      this.tools.openLoader();
    }
    this.commonService.getRideList().subscribe(response => {
      this.tools.closeLoader();
      let res: any = response;
      let resultData = res;
      console.log('Route List ', resultData);

      this.routeList = resultData.ride_list;
      this.today_ride_list = resultData.today_ride_list;
    }, (error: Response) => {
      this.tools.closeLoader();
      let err: any = error;
      if (err && err.status != 0) {
        this.tools.openAlertToken(err.status, err.message);
      }else{        
        this.tools.openAlert('Internet connection not available.');
      }
    });
  }

  openFirst() {
    this.menu.enable(true, 'first');  // replace with MenuA for your case
    this.menu.open('first');
  }
  routeClick(item) {
    this.commonService.setSelectedRoute(item)
    this.router.navigateByUrl('/route-details');
  }
  myProfile() {
    this.menu.close();
    this.router.navigateByUrl('/profile');
  }
  myRideHistory() {
    this.menu.close();
    this.router.navigateByUrl('/ride-history');
  }
  scanCode() {
    if (this.today_ride_list != undefined && this.today_ride_list.length > 0) {
      this.menu.close();
      // Optionally request the permission early
      this.qrScanner.prepare()
        .then((status: QRScannerStatus) => {
          if (status.authorized) {
            // camera permission was granted

            this.scanning = true;
            this.startScanning();
            this.qrScanner.show();
            // start scanning
            let scanSub = this.qrScanner.scan().subscribe((text: string) => {
              console.log('Scanned something', text);
              this.scanning = false;
              this.closeScanner();
              this.stopScanning();
              scanSub.unsubscribe(); // stop scanning
              this.scannedResult = text;
              //changeStatus
              console.log('Today Ride ', this.today_ride_list[0]);
              //  if(this.scannedResult)
              this.callStatusChange(this.today_ride_list[0].MyRideID);
            });

            this.qrScanner.show();
          } else if (status.denied) {
            console.log('Permission Denied');
            this.qrScanner.openSettings();
            // camera permission was permanently denied
            // you must use QRScanner.openSettings() method to guide the user to the settings page
            // then they can grant the permission from there
          } else {
            // permission was denied, but not permanently. You can ask for permission again at a later time.
          }
        })
        .catch((e: any) => console.log('Error is', e));

    } else {
      this.tools.openAlert('You are not scheduled today. Please contact Administrator.');
    }
  }
  startScanning(): void {
    console.log("Started Scanning");
    (window.document.querySelector('html') as HTMLElement).classList.add('cameraView');
  }

  stopScanning(): void {
    console.log("Stopped Scanning");
    (window.document.querySelector('html') as HTMLElement).classList.remove('cameraView');
  }
  closeScanner() {
    this.scanning = false;
    this.qrScanner.hide();
    this.qrScanner.destroy();
  }
  logout() {
    this.tools.presentLogout('Are you sure you want to logout?', 'Logout', 'Cancel');
  }
  callStatusChange(ride_id) {
    this.tools.openLoader();
    this.commonService.changeStatus(ride_id).subscribe(response => {
      this.tools.closeLoader();
      let res: any = response;
      console.log('Success --> ', res);
      this.tools.openAlert(res.message);
      // this.tools.openAlert(JSON.parse(error.error).message);
      // this.today_ride_list = this.today_ride_list[0].Status = 'Ongoing';

    }, (error: Response) => {
      this.tools.closeLoader();
      console.log('Error --> ', error);
      let err: any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }

}
