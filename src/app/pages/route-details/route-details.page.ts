import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-route-details',
  templateUrl: './route-details.page.html',
  styleUrls: ['./route-details.page.scss'],
})
export class RouteDetailsPage implements OnInit {
  selRoute:any;
  constructor(public commonService:CommonService) {
    this.selRoute = this.commonService.getSelectedRoute();
   }

  ngOnInit() {
    
  }

}
