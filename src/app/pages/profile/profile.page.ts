import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  showLicenceImg = false;
  user: any = {};
  constructor(public commonService:CommonService,private tools:Tools) {
    this.user = this.commonService.getUserData();
   }
  ngOnInit() {
  }
  ionViewWillEnter(){
    console.log('ionViewWillEnter --1 ');
  }
  ionViewDidEnter(){
    console.log('ionViewDidEnter --2 ');
    this.getUserInfo();
  }
  getUserInfo() {
    this.tools.openLoader();
    this.commonService.userinfo().subscribe(response => {
      this.tools.closeLoader();
      let res: any = response;
      console.log('User Details --> ',res.data);
      this.commonService.setUserData(res.data,'');
      this.user = this.commonService.getUserData();
    }, (error: Response) => {
      this.tools.closeLoader();
      let err:any = error;
      console.log(err);
      this.tools.openAlertToken(err.status, err.error.message);
    });
  }
  showLicence(){
    this.showLicenceImg = !this.showLicenceImg
    // console.log(this.user.user.PilotLicense);
  }
}
